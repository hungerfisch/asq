#ifndef Wirewrapper_h
#define Wirewrapper_H

#include "Wire.h"

class Wirewrapper  {
public:
    Wirewrapper();
    //uint8_t available();
    //uint8_t receive();
    uint8_t write(uint8_t, uint8_t);
    uint8_t write(int, int);
    uint8_t write(uint8_t, uint8_t, uint8_t); 
    uint8_t write(int, int, int);
    uint8_t write(uint8_t, uint8_t, char*);
    uint8_t write(uint8_t, uint8_t, uint8_t*, uint8_t);
    uint8_t write(uint8_t, uint8_t*, uint8_t);
    uint8_t read(uint8_t, uint8_t);
    uint8_t read(int, int);
    //uint8_t read(uint8_t, uint8_t, uint8_t);
    //uint8_t read(int, int, int);
    //uint8_t read(uint8_t, uint8_t, uint8_t*);
    //uint8_t read(uint8_t, uint8_t, uint8_t, uint8_t*);
private:
    uint8_t returnStatus;
};
#endif
