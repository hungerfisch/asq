#include "Wirewrapper.h"

Wirewrapper::Wirewrapper() {
  Wire.begin();
}

uint8_t Wirewrapper::write( int address, int registerAddress) {
    return(write((uint8_t) address, (uint8_t) registerAddress));
}

uint8_t Wirewrapper::write(int address, int registerAddress, int data)  {
    return(write((uint8_t) address, (uint8_t) registerAddress, (uint8_t) data));
}

uint8_t Wirewrapper::write(uint8_t address, uint8_t registerAddress, char * data) {
    returnStatus = 0;
    returnStatus = write( address, registerAddress, (uint8_t*) data, strlen(data));
    return returnStatus;
}

uint8_t Wirewrapper::write(uint8_t address, uint8_t registerAddress, uint8_t *data, uint8_t numberBytes) {
    returnStatus = 0;
    Wire.beginTransmission(address);
    Wire.write(registerAddress);
    Wire.write(data, numberBytes);
    returnStatus =  Wire.endTransmission();
    return returnStatus;
}

uint8_t Wirewrapper::write(uint8_t address, uint8_t *data, uint8_t numberBytes) {
    returnStatus = 0;
    Wire.beginTransmission(address);
    Wire.write(data, numberBytes);
    returnStatus =  Wire.endTransmission();
    return returnStatus;
}

uint8_t Wirewrapper::write( uint8_t address, uint8_t registerAddress, uint8_t data)   {
    returnStatus = 0;
    Wire.beginTransmission(address);
    Wire.write(registerAddress);
    Wire.write(data);
    returnStatus = Wire.endTransmission();
    return returnStatus;
}

uint8_t Wirewrapper::write(uint8_t address, uint8_t registerAddress)    {
    returnStatus = 0;
    Wire.beginTransmission(address);
    Wire.write(registerAddress);
    returnStatus = Wire.endTransmission();
    return returnStatus;
}

uint8_t Wirewrapper::read(int address, int numberBytes) {
  return read((uint8_t) address, (uint8_t) numberBytes);
}

uint8_t Wirewrapper::read(uint8_t address, uint8_t numberBytes) {
  Wire.requestFrom(address, numberBytes);
  return (uint8_t) Wire.read();
}
